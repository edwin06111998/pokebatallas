/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokebatallas;

/**
 *
 * @author georg_000
 */
public class Pokemon {
    private String Nombre;
    private int Ataque;
    private int Defensa;
    private int nivel;
    private String Especie;
    private String Vivo;

    public Pokemon() {
        Nombre = "";
        Ataque = 0;
        Defensa = 0;
        nivel = 0;
        Especie = "";
        Vivo = "";
    }

    public Pokemon(String Nombre, int Ataque, int Defensa, int nivel, String Especie, String Vivo) {
        this.Nombre = Nombre;
        this.Ataque = Ataque;
        this.Defensa = Defensa;
        this.nivel = nivel;
        this.Especie = Especie;
        this.Vivo = Vivo;
    }
           
    public Pokemon(Pokemon PB){
        this.Nombre = PB.Nombre;
        this.Ataque = PB.Ataque;
        this.Defensa = PB.Defensa;
        this.nivel = PB.nivel;
        this.Especie = PB.Especie;
        this.Vivo = PB.Vivo;
    
    }
         
    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public int getAtaque() {
        return Ataque;
    }

    public void setAtaque(int Ataque) {
        this.Ataque = Ataque;
    }

    public int getDefensa() {
        return Defensa;
    }

    public void setDefensa(int Defensa) {
        this.Defensa = Defensa;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getEspecie() {
        return Especie;
    }

    public void setEspecie(String Especie) {
        this.Especie = Especie;
    }

    public String getVivo() {
        return Vivo;
    }

    public void setVivo(String Vivo) {
        this.Vivo = Vivo;
    }
    
    
    
    
}
