/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokebatallas;

import java.util.ArrayList;

/**
 *
 * @author georg_000
 */
public class PokeBatallas {
    ArrayList<Pokemon> ArrayPokemon;
    ArrayList<Entrenador> ArrayEntrenador;
    
    public PokeBatallas(){
        ArrayPokemon = new ArrayList<Pokemon>();
        ArrayEntrenador = new ArrayList<Entrenador>();
        
    }
    
    public void AgregarPokemon(Pokemon P){
        ArrayPokemon.add(P);
    }
    
    public void AgregarEntrenador(Entrenador E){
        ArrayEntrenador.add(E);
    }
    
    public void ConsultarPokemon(){
        for(Pokemon P : ArrayPokemon){
            System.out.println("Pokemon:" + P.getNombre());
            System.out.println("Ataque: " + P.getAtaque());
            System.out.println("Defensa: " + P.getDefensa());
            System.out.println("Nivel: " + P.getNivel());
            System.out.println("Especie: " + P.getEspecie());
            System.out.println("Vivo: " + P.getVivo());
        }
    }
    
    public void ConsultarEntrenador(){
        for(Entrenador E : ArrayEntrenador){
            System.out.println("Entrenador:" + E.getNombre());
            System.out.println("Edad: " + E.getEdad());
            System.out.println("Pueblo: " + E.getPueblo());
            System.out.println("Lider de gimnasio: " + E.getEsLiderGimnasio());
        }
        
    }
}

