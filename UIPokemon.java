/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UIPokeBatallas;

import java.util.Scanner;
import pokebatallas.Entrenador;
import pokebatallas.PokeBatallas;
import pokebatallas.Pokemon;

/**
 *
 * @author georg_000
 */
public class UIPokemon {
    PokeBatallas PB = new PokeBatallas();
    
    public void RegistroPokemon(){
        //Ingreso de la información del pokemon
        Scanner sc = new Scanner(System.in);
        System.out.println("Pokedex");
        System.out.println("Nombre del pokemon: ");
        String Nombre = sc.nextLine();
        System.out.println("Especie del pokemon: ");
        String Especie = sc.nextLine();
        System.out.println("El pokemon está vivo?");
        String Vivo = sc.nextLine();
        System.out.println("Ataque del pokemon: ");
        int Ataque = sc.nextInt();
        System.out.println("Defensa del pokemon: ");
        int Defensa = sc.nextInt();
        System.out.println("Nivel del pokemon: ");
        int nivel = sc.nextInt();
        //Llamada al constructor de la clase Pokemon
        Pokemon P = new Pokemon(Nombre,Ataque,Defensa,nivel,Especie,Vivo);
        //ArrayList para almacenar los pokemones
        PB.AgregarPokemon(P);
        
    }
    
    public void RegistroEntrenador(){
        //Ingreso de la información del pokemon
        Scanner sc = new Scanner(System.in);
        System.out.println("Pokemon Yellow");
        System.out.println("Nombre del entrenador: ");
        String Nombre = sc.nextLine();
        System.out.println("Edad del entrenador: ");
        int edad = sc.nextInt();
        System.out.println("Pueblo: ");
        String Pueblo = sc.nextLine();
        System.out.println("¿Es lider de Gimnasio?: ");
        String esLiderGimnasio = sc.nextLine();
        //Llamada al constructor de la clase Entrenador
        Entrenador E = new Entrenador(Nombre,edad,Pueblo,esLiderGimnasio);
        //ArrayList para almacenar los entrenadores
        PB.AgregarEntrenador(E);
        
    }
    
}
